package ADailyLibrary

import (
	"github.com/tidwall/gjson"
	"testing"
)

const json = `{"name":{"first":"Janet","last":"Prichard"},"age":47}`

func TestJson(t *testing.T) {
	value := gjson.Get(json, "name")
	println(value.String())
	results := gjson.GetMany(json, "name.last", "age")
	for _, result := range results {
		println(result.String())
	}
	value = gjson.GetBytes([]byte(json), "name.last")
	println(value.Raw)
}

const json1 = `{"name":{"name":"zhangsan","age":47},"name1":{"name":"lisi","age":42}}`

func TestJson2(t *testing.T) {
	gjson.Get(json1,"name1").ForEach(func(key, value gjson.Result) bool{
		println(key.String(),":",value.String())
		return true
	})

	println(gjson.Parse(gjson.Get(json1,"name").String()).String())
}

const jsonArray = `{
  "names": [
    {
      "name": "zhangsan"
    },
    {
      "name": "lisi"
    }
  ]
}`

func TestJson3(t *testing.T) {
	value := gjson.Get(jsonArray,"names.0")
	value2 := gjson.Get(jsonArray,"names.1.name")
	println(value.Get("name").String())
	println(value2.String())
}
