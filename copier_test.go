package ADailyLibrary

import (
	"fmt"
	"github.com/jinzhu/copier"
	"testing"
)

type User struct {
	Name string
	Age int
	Role string
}

// 源对象没用同名字段，但有同名方法，Copy会调用方法将返回值赋给目标对象字段
func (u *User) DoubleAge() int{
	return 2 * u.Age
}

type Employee struct {
	Name string
	Age int
	DoubleAge int
	SuperRole string
}

// 目标对象方法与源对象字段同名，Copy会调用这个方法将源对象同名字段当参数传进来
func (e *Employee) Role(role string)  {
	e.SuperRole = "Super" + role
}

// 方法赋值
func TestFunc(t *testing.T) {
	user := User{Name: "xx",Age: 18,Role: "Admin"}
	employee := Employee{}
	copier.Copy(&employee,&user)
	fmt.Printf("%#v\n",employee)
}

// 切片赋值
func TestSlice(t *testing.T) {
	users := []User{
		{Name: "xx",Age: 18},
		{Name: "xx",Age: 18},
	}
	employees := []Employee{}

	copier.Copy(&employees,&users)
	fmt.Printf("%#v\n",employees)
}

// 结构体赋值到切片
func TestStructToSlice(t *testing.T)  {
	user := User{Name: "dj", Age: 18}
	employees := []Employee{}

	copier.Copy(&employees, &user)
	fmt.Printf("%#v\n", employees)
}

type Class struct {
	Name string
	ID int
}

type Student struct {
	ID int
	Name string
	No string
	Class Class
}

type Student2 struct {
	ID int
	Class Class
}

func TestDemo(t *testing.T) {
	student := Student{ID: 1,Name: "xx",Class: Class{Name:"1 班",ID: 2}}
	student2 := Student2{}
	copier.Copy(&student2,student)
	fmt.Printf("%#v\n", student2)
}