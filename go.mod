module Library

go 1.16

require (
	github.com/jinzhu/copier v0.3.2 // indirect
	github.com/tidwall/gjson v1.8.1
	github.com/tidwall/pretty v1.2.0 // indirect
)
